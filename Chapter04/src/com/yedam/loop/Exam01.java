package com.yedam.loop;

public class Exam01 {
	public static void main(String[] args) {
		int sum = 0;
		//1) 규칙
		sum = sum + 1; // sum = 0 + 1
		sum = sum + 2; // sum = 1 + 2
		sum = sum + 3; // sum = 2 + 3
		sum = sum + 4; // sum = 3 + 4
		sum = sum + 5; // sum = 4 + 5
		
		// 1~5까지의 합을 구하는 반복문
		for (int i=0; i<=5; i++) {
			sum = sum+i;
//			System.out.println(sum);
		}
		
		// 짝수 구하는 반복문
		for (int i=0; i<=100; i++) {
			if(i%2==0) { // 짝수
//				System.out.println(i);
			}
		}
		
		// 1~100 사이에서 홀수 구하는 반복문
		// 1) 규칙
		for (int i=100; i>=1; i--) {
			if(i%2==1) {
//				System.out.println(i);
			}
		}
		
		// 1~ 100사이에서 2의 배수 또는 3의 배수 찾기
		// 규칙
		// 2의 배수 i % 2 == 0
		// 3의 배수 i % 3 == 0
		for(int i=0; i<=100; i++) {
			if(i % 6 == 0) {
//				System.out.println(i + "는 2의 배수 또는 3의 배수입니다");
			}
		}
		
		// 구구단 출력
		// 2단 출력
		// 2*1, 2*2, 2*3...9
		for (int i=2; i<10; i++) {
//			System.out.println("2 x " + i + " = " + (2*i));
		}
		int sum2 = 0;
		for (int i=2; i<=9; i++) {
			// i가 2일때 아래 반복문은 9번 돌아감
			for(int j=1; j<=9; j++) {
				sum = i*j;
//				System.out.println(i + " * " + j + " = " + sum);
			}
		}
		
		// 공포의 별찍기
		// *****
		// *****
		// *****
		// *****
		// *****
		
		// *
		// **
		// ***
		// ****
		// *****
		for(int i=0; i<=5; i++) {
			String star ="";
			for(int j=0; i>j; j++) {
				star = star + "*";
			}
//			System.out.println(star);
		}
		
		// *****
		// ****
		// ***
		// **
		// *
		for(int i=0; i<=5; i++) {
			String star ="";
			for(int j=5; i<j; j--) {
				star = star + "*";
			}
//			System.out.println(star);
		}
		
		//     * 4 , 1
		//    ** 3 , 2
		//   *** 2 , 3
		//  **** 1 , 4
		// ***** 0 , 5
		for (int i=5; i>0; i--) {
			String star="";
			for(int j=1; j<6; j++) {
				if (j >= i) {
					star = star + "*";
				} else {
					star = star + " ";
				}
			}
			System.out.println(star);
		}
	}
}
