package com.yeadm.homework;

public class Exam04 {
	public static void main(String[] args) {
		// 4
		int intResult = 0+15;
		intResult = (intResult > -1) ? intResult : 0;
		System.out.println(intResult);
		// 5
		int a = 10;
		int b = -8;
		String strResult;
		
		if (a > -1 && b > -1) {
			strResult = "both a and b are zero or more";
			System.out.println(strResult);
		} else {
			strResult = "One of a or b is negative number";
			System.out.println(strResult);
		}
	}
}
