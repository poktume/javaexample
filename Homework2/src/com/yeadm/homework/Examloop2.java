package com.yeadm.homework;

import java.util.Scanner;

public class Examloop2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// 연도가 주어졌을때 해당 년도가 윤년인지 확인후 출력
		// 윤년은 연도가 4의 배수이며, 100의 배수가 아닐때 혹은 400의 배수일때
		int year;
		System.out.println("년 입력");
		year = sc.nextInt();
		
		if (year % 4 == 0) {
			if (year % 100 != 0) {
				System.out.println("윤년입니다");
			} else {
				System.out.println("윤년이 아닙니다");
			}
		} 
	}
}
