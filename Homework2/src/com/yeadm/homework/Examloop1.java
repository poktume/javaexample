package com.yeadm.homework;

import java.util.Scanner;

public class Examloop1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// 1
		int x;
		int y;
		
		
		System.out.println("값 입력");
		x = sc.nextInt();
		
		System.out.println("값 입력");
		y = sc.nextInt();
		
		if ( x>0 && y>0 ) {
			System.out.println("제 1사분면 입니다.");
		} else if ( x<0 && y>0) {
			System.out.println("제 2사분면 입니다.");
		} else if ( x<0 && y<0) {
			System.out.println("제 3사분면 입니다.");
		} else if ( x>0 && y<0) {
			System.out.println("제 4사분면 입니다.");
		}
		
		// 2
		// 연도가 주어졌을때 해당 년도가 윤년인지 확인후 출력
		// 윤년은 연도가 4의 배수이며, 100의 배수가 아닐때 혹은 400의 배수일때
		int year;
		System.out.println("년 입력");
		year = sc.nextInt();
		
		if (year % 4 == 0) {
			if (year % 100 != 0) {
				System.out.println("윤년입니다");
			} else {
				System.out.println("윤년이 아닙니다");
			}
		}
		
		// 3
		int count = 5;
		int randomNo = (int) (Math.random() * 50) + 1;
		System.out.println(randomNo);
		while (true) {
			System.out.println("숫자 입력 : ");
			int num = Integer.parseInt(sc.nextLine());
			if ( randomNo == num ) {
				System.out.println("정답입니다.");
				break;
			} else if ( randomNo < num) {
				System.out.println("업, 틀렸습니다");
				count -= 1;
			} else if ( randomNo > num) {
				System.out.println("다운, 틀렸습니다");
				count -= 1;
			} if (count == 0) {
				System.out.println("기회가 없습니다, 종료합니다.");
				break;
			}
		}
		
		// 4
		int m = 0;
		int n = 0;
		System.out.println("앞단 입력 :");
		m = Integer.parseInt(sc.nextLine());
		System.out.println("뒷단 입력 :");
		n = Integer.parseInt(sc.nextLine());
		for (int i=1; i<=n; i++) {
			int score = m * i;
			System.out.println(m + " X " + i + " = " + score);
		}
	}
}
