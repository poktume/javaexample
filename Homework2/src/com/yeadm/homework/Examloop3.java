package com.yeadm.homework;

import java.util.Scanner;

public class Examloop3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int count = 5;
		int randomNo = (int) (Math.random() * 50) + 1;
		System.out.println(randomNo);
		while (true) {
			System.out.println("숫자 입력 : ");
			int num = Integer.parseInt(sc.nextLine());
			if ( randomNo == num ) {
				System.out.println("정답입니다.");
				break;
			} else if ( randomNo < num) {
				System.out.println("업, 틀렸습니다");
				count -= 1;
			} else if ( randomNo > num) {
				System.out.println("다운, 틀렸습니다");
				count -= 1;
			} if (count == 0) {
				System.out.println("기회가 없습니다, 종료합니다.");
				break;
			}
		}
	}
}
