package com.yedam.study;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//두 개의 주사위를 던졌을때, 눈의 합이 6이 되는 모든 경우의 수를
		//출력하는 프로그램 구현
		//1, 5
		//2, 4
		//3, 3
		//4, 2
		//5, 1
		//A 주사위 B주사위
		//A 주사위가 1 일때, B 주사위가 1~6까지의 합을 구한 다음
		//합의 결과가 6이면 내가 원하는 조건의 만족
		// 중첩 for문이 정답
		
		
		// 숫자를 하나 입력 받아, 양수인지 음수인지 출력
		// 단 0이면 0입니다라고 출력
		// 스캐너, 조건문이 정답
		
		//정수 두개와 연산기호 1개를 입력 받아서
		//연산 기호에 해당되는 계산을 수행하고 출력
		int sum = 0;
		System.out.println("정수 입력");
		int a = 0;
		a = Integer.parseInt(sc.nextLine());
		System.out.println("정수 입력");
		int b = 0;
		b = Integer.parseInt(sc.nextLine());
		System.out.println("연산 기호 입력");
		String str;
		str = sc.nextLine();
		if (str.equals("+")) {
			sum = a+b;
		} else if (str.equals("-")) {
			sum = (a-b);
		} else if (str.equals("*")) {
			sum = a*b;
		} else if (str.equals("/")) {
			sum = a/b;
		} else {
			System.out.println("잘못된 입력");
		}
		System.out.println(-sum);
	}
}
