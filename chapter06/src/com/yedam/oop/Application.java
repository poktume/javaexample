package com.yedam.oop;

public class Application {
	public static void main(String[] args) {
		// SmartPhone 클래스(설계도)를 토대로 iphone14Pro 생성
		SmartPhone iphone14Pro = new SmartPhone("Apple", "iphone14Pro", 500);
//		iphone14Pro.maker = "Apple";
//		iphone14Pro.name = "iphone14Pro";
//		iphone14Pro.price = 100000;
		
		iphone14Pro.call();
		iphone14Pro.hangUp();
		
		//필드 정보 읽기
		System.out.println(iphone14Pro.maker);
		System.out.println(iphone14Pro.name);
		System.out.println(iphone14Pro.price);
		
		//SmartPhone 클래스(설계도)
//		SmartPhone zflip4 = new SmartPhone("samsung", "zflip4", 1000);
////		zflip4.maker = "samsung";
////		zflip4.name = "zflip4";
////		zflip4.price = 100000;
//		
//		zflip4.call();
//		zflip4.hangUp();
//		
//		System.out.println(zflip4.maker);
		SmartPhone sony = new SmartPhone();
		
		//리턴 타입이 없는 메소드
		sony.getInfo(0);
		//리턴 타입이 int인 메소드
		int b = sony.getInfo("int");
		//리턴 타입이 String[]인 메소드
		String[] temp = sony.getInfo(args);
	}
}
