package com.yedam.oop;

public class Student {


	String name;
	String sch;
	String num;
	int kor;
	int math;
	int eng;
	
	
	// 객체 필드에 접근하여 필드 초기화
	public Student() {
		
	}
	// 생성자를 통한 필드 초기화
	public Student(String name,String sch, String num) {
		this.name = name;
		this.sch = sch;
		this.num = num;
//		this.kor = kor;
//		this.math = math;
//		this.eng = eng;
	}

	void getInfo() {
		System.out.println();
		System.out.println("학생의 이름 : " + name);
		System.out.println("학생의 학교 : " + sch);
		System.out.println("학생의 학번 : " + num);
		System.out.println("총 점 : " + totalScore());
		System.out.println("평 균 : " + avg());
		
	}
	
	int totalScore() {
		return kor+math+eng;
	}
	double avg() {
		double avgs = totalScore() / (double)3;
		return avgs;
	}
}
