package com.yedam.exam01;

public class Application {
	public static void main(String[] args) {
		Student std1 = new Student();
		
		std1.setStdName("김또치");
		std1.setStdGrade("2학년");
		std1.setMajor("컴퓨터공학과");
		std1.setPrograming(50);
		std1.setDatabase(60);
		std1.setOS(90);
		
		System.out.println("이름 : " + std1.getStdName());
		System.out.println("학과 : " + std1.getMajor());
		System.out.println("학년 : " + std1.getStdGrade());
		System.out.println("프로그래밍 언어 점수 : " + std1.getPrograming());
		System.out.println("데이터베이스 점수 : " + std1.getDatabase());
		System.out.println("운영체제 점수 : " + std1.getOS());
		
	}
}
