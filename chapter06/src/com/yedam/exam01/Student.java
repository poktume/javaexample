package com.yedam.exam01;

public class Student {
	private String stdName;
	private String major;
	private String stdGrade;
	private int programing;
	private int database;
	private int OS;
	
	//클래스를 통한 객체를 생성할때 첫번째로 수행하는 일들을 모아두는 곳.
	//필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
	//생성자에서 this키워드를 활용해서 필드 초기화 하면 됨.
	
	public String getStdName() {
		return stdName;
	}

	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStdGrade() {
		return stdGrade;
	}

	public void setStdGrade(String stdGrade) {
		this.stdGrade = stdGrade;
	}

	public int getPrograming() {
		return programing;
	}

	//프로그래밍 언어 점수가 0보다 작은 점수가 들어올 경우
	//프로그래밍 언어 점수를 0으로 처리하겠다.
	public void setPrograming(int programing) {
		if(programing <= 0) {
			this.programing = 0;
		}
		this.programing = programing;
	}

	public int getDatabase() {
		return database;
	}

	public void setDatabase(int database) {
		this.database = database;
	}

	public int getOS() {
		return OS;
	}

	public void setOS(int oS) {
		OS = oS;
	}
}
 