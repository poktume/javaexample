package com.yedam.inter;

public interface GetInfo {

	//넓이를 구하는 메소드
	void area();
	
	//둘레를 구하는 메소드
	void round();
}
