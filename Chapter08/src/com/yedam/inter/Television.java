package com.yedam.inter;

public class Television implements RemoteControl{
	//필드
	private int volume;
	//생성자
	
	//메소드
	@Override
	public void turnOn() {
		System.out.println("전원을 켭니다.");
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끕니다.");
	}

	@Override
	public void setVolume(int volume) {
		//최대 소리 이상으로 데이터가 들어 올 때
		if(volume > RemoteControl.MAX_VOLUME) {
			this.volume = RemoteControl.MAX_VOLUME;
			System.out.println("이미 최대 볼륨입니다.");
		}
		//최소 소리 이하로 데이터가 들어 올 때
		else if (volume < RemoteControl.MIN_VOLUME) {
			this.volume = RemoteControl.MIN_VOLUME;
			System.out.println("이미 최소 볼륨입니다.");
		} else { // 정상 소리 범주
			this.volume = volume;
		}
		System.out.println("현재 TV 볼륨 : " + this.volume);
	}

	@Override
	public void search(String url) {
		// TODO Auto-generated method stub
		
	}
}
