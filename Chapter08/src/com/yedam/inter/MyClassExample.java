package com.yedam.inter;

public class MyClassExample {

	public static void main(String[] args) {
		//1)
		System.out.println("1)==========================");
		
		MyClass myClass = new MyClass();
		
		myClass.rc.turnOn();
		myClass.rc.turnOff();
		//2)
		System.out.println("2)==========================");
		
		MyClass myClass2 = new MyClass(new Audio());
		//3)
		System.out.println("3)==========================");
		
		MyClass myClass3 = new MyClass();
		myClass3.method1();
		//4)
		System.out.println("4)==========================");
		MyClass myClass4 = new MyClass();
		myClass4.methodB(new Television());
	}

}
