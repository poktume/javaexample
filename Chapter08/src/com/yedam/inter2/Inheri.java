package com.yedam.inter2;

public class Inheri {
	public static void main(String[] args) {
		// A <-B <-D ( + A <- D )
		// I   C   C
		// A <- B
		A a = new B();
		a.info();
		// A <- D
		A a2 = new D();
		a2.info();
	}
}
