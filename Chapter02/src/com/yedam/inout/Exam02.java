package com.yedam.inout;

public class Exam02 {
	public static void main(String[] args) {
		// 문제 1
		int i = 37;
		int x = 91;
		
		System.out.println("변수명1 : " + i);
		System.out.println("변수명2 : " + x);
		
		// 문제 2
		System.out.println("2.1) " + (x + i));
		System.out.println("2.2) " + (x - i));
		System.out.println("2.3) " + (x * i));
		System.out.println("2.4) " + (x / i));
		
		// 문제3
		int var1 = 128;
		String var2 = "B";
		char var3 = 44032;
		long var4 = 100000000000L;
		double var5 = 43.93106;
		float var6 = 301.3f;
		
		System.out.printf("3-1) %d ,int\n", var1);
		System.out.printf("3-2) %s ,String\n", var2);
		System.out.printf("3-3) %c ,char\n", var3);
		System.out.printf("3-4) %d ,long\n", var4);
		System.out.printf("3-5) %.5f ,double\n", var5);
		System.out.printf("3-6) %.1f ,float\n", var6);
		
		// 문제4
		byte a = 35;
		byte b =25;
		int c = 463;
		long d = 1000L;
		
		int result1 = a + c + (int)d;
		System.out.println("4-1) " + result1);
		
		int result2 = a + b + c;
		System.out.println("4-2) " + result2);
		
		double e = 45.31;
		double result3 = c + d + e;
		System.out.println("4-3) " + result3);
		
		// 문제5
		int intValue1 = 24;
		int intValue2 = 3;
		int intValue3 = 8;
		int intValue4 = 10;
		char charValue = 'A';
		String strValue = "번지";
		
		System.out.printf("%c%d%d%s%.1f\n",charValue,intValue1+intValue2,intValue3,strValue,(double)intValue4);
		System.out.println(String.valueOf(charValue) + (intValue1 + intValue2) + intValue3 + strValue + (double)intValue4);
		
		// 추가문제
		int value = 485;
		int result = 0;
		
		while(value>0) {
			result += value % 10;
			value /= 10;
		}
		System.out.println(result);
		
		int hundred = value / 100; // 4
		int ten = (value - (100*hundred) / 10); // 8
		int one = (value - (100*hundred) - (10*ten));
		
		int intResult = hundred + ten + one;
		System.out.println(intResult);
	}
}
