package com.yedam.exam;


// Human 클래스를 상속한다.
public class StandardWeightInfo extends Human{
	
	public StandardWeightInfo(String name, double height, double weight) {
		super(name, height, weight);
	}
	
	// - 메소드는 다음과 같이 정의한다.
	//
	public void getInformation() {
		super.getInformation();
		//1번 방식
		System.out.println(" 표준체중 : " + getStandardWeight());
		//2번 방식 출력 -> 61.2
//		System.out.printf("표준체중 %.1f 입니다.\n", getStandardWeight());
	}
	
	public double getStandardWeight() {
		double sw = ((height - 100) * 0.9);
		return sw;
	}
	
}
