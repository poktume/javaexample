package com.yedam.exam;

public class Employee {
	//필드
	public String name;
	public int salary;
	//생성자
	public Employee(String name, int salary) {
		this.name = name;
		this.salary = salary;
	}
	
	//메소드
	public String getName() {
		return name;
	}
	public int getSalary() {
		return salary;
	}
	
	void getInformation() {
		System.out.print("이름 : " + name + " 연봉 : " + salary);
	}
	
	void print() {
		System.out.println("슈퍼클래스");
	}
}
