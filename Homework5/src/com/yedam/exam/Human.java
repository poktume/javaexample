package com.yedam.exam;

public class Human {
	//필드
	//이름과 키 몸무게를 필드로 가지며,
	String name;
	double height;
	double weight;
	//생성자를 이용하여 값을 초기화 한다.
	public Human(String name, double height, double weight) {
		this.name = name;
		this.height = height;
		this.weight = weight;
	}
	//메소드
	void getInformation() {
		System.out.print(name + "님의 신장 " + height + ", 몸무게 "+ weight + " 입니다.");
	}
}
