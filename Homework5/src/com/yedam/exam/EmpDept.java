package com.yedam.exam;

public class EmpDept extends Employee{
	//필드
	public String deptName;
	//생성자
	public EmpDept(String name, int salary, String deptName) {
		super(name, salary);
		this.deptName = deptName;
	}
	
	//메소드
	public String getDeptName() {
		return deptName;
	}
	
	@Override
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : " + deptName);
	}
	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
}
