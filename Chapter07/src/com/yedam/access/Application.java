package com.yedam.access;

public class Application {
	public static void main(String[] args) {
		
		Child child = new Child();
		
		child.lastName = "또치";
		child.age = 20;
		
//		System.out.println("내 이름 : " + child.fistName + child.lastName);
//		System.out.println("DNA : " + child.DNA);
		//Parent 클래스 -> bloodType을 private 설정
		//Child 클래스 -> Parent클래스의 bloodType 사용 X
//		System.out.println("혈액형 : " + child.bloodType);
		child.showInfo();
		System.out.println("나이 : " + child.age);
		System.out.println();
	}
}
