package com.yedam.access;

import com.yedam.inheri.Parent;

public class Child extends Parent{
	public String lastName;
	public int age;
	
	public Child() {
		
	}
	
	public void showInfo() {
		System.out.println("내 성씨 : " + firstName);
		System.out.println("DNA : " + DNA);
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
