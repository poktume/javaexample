package com.yedam.inheri;

public class Parent {
	//부모 클래스
	//1) 상속 할 필드 정의
	protected String firstName = "Lee";
	protected String lastName;
	protected String DNA = "B";
	//2) 상속 대상에서 제외
//	private char bloodType = 'B';
	public int age;
	
	//오버라이딩 예제
	public void method1 () {
		System.out.println("parent class -> method1 Call");
	}
	public void method2 () {
		System.out.println("parent class -> method2 Call");
	}
}
