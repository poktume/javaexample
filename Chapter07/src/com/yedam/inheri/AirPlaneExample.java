package com.yedam.inheri;

public class AirPlaneExample {

	public static void main(String[] args) {
		SuperSonicAirplane sa = new SuperSonicAirplane();
		
		sa.takeOff();
		
		//부모 클래스를 그대로 받아 사용하는것
		sa.fly();
		
		sa.flyMode = SuperSonicAirplane.SUPERSONIC;
		
		sa.fly();
		
		sa.flyMode = SuperSonicAirplane.NORMAR;
		
		sa.fly();
		
		sa.land();
	}

}
