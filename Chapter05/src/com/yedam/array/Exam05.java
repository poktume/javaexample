package com.yedam.array;

import java.util.Scanner;

public class Exam05 {
	public static void main(String[] args) {
		
		//학생수, 학생점수 받아오고 최고점수 및 평균점수 구하기
		boolean run = true;
		int studentNum = 0;
		int[] scores = null;
		Scanner sc = new Scanner(System.in);
		
		while(run) {
			System.out.println("========================================================");
			System.out.println("1. 학생수 | 2. 점수입력 | 3. 점수리스트 | 4. 분석 | 5. 종료");
			System.out.println("========================================================");
			System.out.println("선택>");
			
			int selectNo = Integer.parseInt(sc.nextLine());
			
			if(selectNo == 1) {
				//배열 크기(방의 갯수) 지정
				System.out.println("학생수를 입력>");
				studentNum = Integer.parseInt(sc.nextLine());
				
			} else if (selectNo == 2) {
				scores = new int[studentNum];
				
				for(int i=0; i<scores.length; i++) {
					System.out.println("점수를 입력>");
					scores[i] = Integer.parseInt(sc.nextLine());
				}
				
			} else if (selectNo == 3) {
				System.out.println("점수리스트>");
				for(int i=0; i<scores.length; i++) {
					System.out.println("socres[" + i +"]> " +scores[i]);
				}
				
			} else if (selectNo == 4) {
				System.out.println("분석");
				int max = scores[0];
				int sum = 0;
				
				for(int i=0; i<scores.length; i++) {
					if (max < scores[i]) {
						max = scores[i];
					}
					sum = sum + scores[i];
				}
					System.out.println("최고점수 : " + max);
					System.out.println("평균점수 : " + (double)sum / scores.length);
			} else if (selectNo == 5) {
					run = false;
				}
		System.out.println("프로그램 종료");
		}
	}
}
