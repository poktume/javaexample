package com.yedam.array;

import java.util.Scanner;

public class Exam04 {
	public static void main(String[] args) {
		
	
	//Lotto - 보류
	//배열에 담긴 값 중에서 최대값, 최소값 구하기
	Scanner sc = new Scanner(System.in);
	
	int[] ary;
	int no;
	
	System.out.println("배열의 크기>");
	no = Integer.parseInt(sc.nextLine());
	ary = new int[no];
	
	for(int i=0; i<ary.length; i++) {
		System.out.println("입력>");
		ary[i] = Integer.parseInt(sc.nextLine());
	}
	int max=0;
	int min = ary[0];
	for(int i=0; i<ary.length; i++) {
		// 최대 값
		if (max < ary[i]) {
			max = ary[i];
		}
		// 최소 값
		if (min > ary[i]) {
			min = ary[i];
		}
	}
	System.out.println("최대 값 : " + max);
	System.out.println("최소 값 : " + min);
	
	}
}
