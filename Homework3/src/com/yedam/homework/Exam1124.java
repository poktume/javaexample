package com.yedam.homework;

import java.util.Scanner;

public class Exam1124 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		// 메뉴 선택 입력값 받아오기
		int selectNo = Integer.parseInt(sc.nextLine());
		// 배열 필드 생성
		ProductExam[] pd = null;
		// 값 필드 생성
		int product = 0;
		while(true) {
			System.out.println("==========================================================");
			System.out.println("1. 상품 수 | 2.상품 및 가격입력 | 3. 제품별 가격 | 4. 분석 | 5. 종료");
			System.out.println("==========================================================");
			System.out.println("메뉴를 선택하세요>");
			selectNo = Integer.parseInt(sc.nextLine());
			
			if(selectNo == 1) { 
				System.out.println("상품 수를 입력하세요>");
				product = Integer.parseInt(sc.nextLine());
				
			} else if (selectNo == 2) {
				ProductExam products = new ProductExam();
				System.out.println("상품 및 가격을 입력하세요>");
				for(int i=0; i<pd.length; i++) {
					// 상품 객체 생성
					ProductExam p1 = new ProductExam();
					System.out.println((i+1)+"번째 상품");
					System.out.println("상품명>");
					
					// 변수에 데이터를 입력받고 객체에 데이터를 넣는방법
					String name = sc.nextLine();
					p1.name = name;
					
					System.out.println("가격>");
					// 데이터를 입력 받음과 동시에 객체에 데이터를 넣는 방법
					p1.price = Integer.parseInt(sc.nextLine());
					
					pd[i]= p1;
					System.out.println("===============");
				}
			} else if (selectNo == 3) {
				// 배열의 크기만 반복문을 진행할때 배열에 각 인덱스(방번호)를
				// 활용하여 객체(상품)을 꺼내와서 객체(상품)에 정보를 하나씩 꺼내옴.
				for(int i=0; i<pd.length; i++) {
					// 상품 명
					String name = pd[i].name;
					// 상품 가격
					int price = pd[i].price;
					
					System.out.println("상품 명 : " + name);
					System.out.println("상품 가격 : " + price);
				}
			} else if (selectNo == 4) {
				int max = pd[0].price;
				int sum = 0;
				for(int i=0; i<pd.length; i++) {
					
					//제일 비싼 상품 가격 구하기
					if ( max < pd[i].price) {
						max = pd[i].price;
					}
					//상품들의 가격 합계 구하기
					sum += pd[i].price;
				}
				System.out.println("제일 비싼 상품 가격 : " + max);
				System.out.println("제일 비싼 상품을 제외한 상품 총 합: " + (sum - max));
			} else if(selectNo == 5) {
				System.out.println("프로그램 종료");
				break;
			}
		}
	}
}
