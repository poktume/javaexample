package com.yedam.homework;

public class ProductExam {

	String name;
	int price;
	
	public ProductExam() {
		
	}
	public ProductExam(String name, int price ) {
		this.name = name;
		this.price = price;
	}
	void getInfo() {
		System.out.println("상품명 : " + name);
		System.out.println("가격 : " + price);
	}
}
